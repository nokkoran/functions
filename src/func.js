const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' || typeof str2 !== 'string' || isNaN(str1) || isNaN(str2)) {
    return false;
  }
  if(str1.length === 0) {
    str1 = "0";
  }
  if(str2.length === 0) {
    str1 = "0";
  }
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for(let i of listOfPosts) {
    if(i.author === authorName) {
      posts++;
    }
    if(i.comments == undefined) {
      continue;
    }
    for(let j of i.comments) {
      if(j.author === authorName) {
        comments++;
      }
    }
  }
  return "Post:" + posts + "," + "comments:" + comments;
};

const tickets=(_people)=> {
  let avaliable = 0;
  for(let i of _people) {
    if(i === 25) {
      avaliable++;
    }
    if(i === 50) {
      if(avaliable > 0) {
        avaliable--;
        avaliable++;
      }
    }
    if(i === 100) {
      if(avaliable > 1) {
        avaliable -= 2;
      } else {
        return "NO";
      }
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
